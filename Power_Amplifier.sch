EESchema Schematic File Version 4
LIBS:pqws-main-hw-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:L L3
U 1 1 5A7D1DA8
P 7200 3200
F 0 "L3" V 7150 3200 50  0000 C CNN
F 1 "12nH" V 7275 3200 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad0.84x1.00mm_HandSolder" H 7200 3200 50  0001 C CNN
F 3 "" H 7200 3200 50  0001 C CNN
F 4 "HK160812NJ-T" V 7200 3200 60  0001 C CNN "Part Number"
	1    7200 3200
	0    -1   -1   0   
$EndComp
$Comp
L Device:L L5
U 1 1 5A7D1DAF
P 7500 3900
F 0 "L5" V 7450 3900 50  0000 C CNN
F 1 "5.6nH" V 7575 3900 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad0.84x1.00mm_HandSolder" H 7500 3900 50  0001 C CNN
F 3 "" H 7500 3900 50  0001 C CNN
F 4 "LQG18HN5N6S00D" V 7500 3900 60  0001 C CNN "Part Number"
	1    7500 3900
	0    -1   -1   0   
$EndComp
$Comp
L Device:L L4
U 1 1 5A7D1DB6
P 7350 4200
F 0 "L4" V 7300 4200 50  0000 C CNN
F 1 "22nH" V 7425 4200 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad0.84x1.00mm_HandSolder" H 7350 4200 50  0001 C CNN
F 3 "" H 7350 4200 50  0001 C CNN
F 4 "LQW18AN22NG00D" V 7350 4200 60  0001 C CNN "Part Number"
	1    7350 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:L L1
U 1 1 5A7D1DBD
P 4800 4100
F 0 "L1" V 4750 4100 50  0000 C CNN
F 1 "3.9nH" V 4875 4100 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad0.84x1.00mm_HandSolder" H 4800 4100 50  0001 C CNN
F 3 "" H 4800 4100 50  0001 C CNN
F 4 "B82496C3399J" V 4800 4100 50  0001 C CNN "Part Number"
	1    4800 4100
	0    -1   -1   0   
$EndComp
$Comp
L Device:L L2
U 1 1 5A7D1DC4
P 5250 2700
F 0 "L2" V 5200 2700 50  0000 C CNN
F 1 "5.6nH" V 5325 2700 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad0.84x1.00mm_HandSolder" H 5250 2700 50  0001 C CNN
F 3 "" H 5250 2700 50  0001 C CNN
F 4 "LQG18HN5N6S00D" V 5250 2700 60  0001 C CNN "Part Number"
	1    5250 2700
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C3
U 1 1 5A7D1DCB
P 7050 3900
F 0 "C3" H 7075 4000 50  0000 L CNN
F 1 "220pF" H 7075 3800 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad0.84x1.00mm_HandSolder" H 7088 3750 50  0001 C CNN
F 3 "" H 7050 3900 50  0001 C CNN
F 4 "GRM1555C1H221JA01ND" H 7050 3900 50  0001 C CNN "Part Number"
	1    7050 3900
	0    1    1    0   
$EndComp
$Comp
L Device:C C11
U 1 1 5A7D1DD2
P 7900 3350
F 0 "C11" H 7925 3450 50  0000 L CNN
F 1 "4.7uF" H 7925 3250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad0.84x1.00mm_HandSolder" H 7938 3200 50  0001 C CNN
F 3 "" H 7900 3350 50  0001 C CNN
F 4 "GRM035R60J475ME15D" H 0   0   50  0001 C CNN "Part Number"
	1    7900 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C12
U 1 1 5A7D1DD9
P 7500 3350
F 0 "C12" H 7525 3450 50  0000 L CNN
F 1 "1000pF" H 7525 3250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad0.84x1.00mm_HandSolder" H 7538 3200 50  0001 C CNN
F 3 "" H 7500 3350 50  0001 C CNN
F 4 "GCD188R71H102KA01D" H 0   0   50  0001 C CNN "Part Number"
	1    7500 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5A7D1DE0
P 3500 3450
F 0 "C8" H 3525 3550 50  0000 L CNN
F 1 "1uF" H 3525 3350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad0.84x1.00mm_HandSolder" H 3538 3300 50  0001 C CNN
F 3 "" H 3500 3450 50  0001 C CNN
F 4 "GRM188R61A105KA61ND" H 3500 3450 60  0001 C CNN "Part Number"
	1    3500 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C10
U 1 1 5A7D1DE7
P 5450 2900
F 0 "C10" H 5475 3000 50  0000 L CNN
F 1 "2.4pF" H 5475 2800 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad0.84x1.00mm_HandSolder" H 5488 2750 50  0001 C CNN
F 3 "" H 5450 2900 50  0001 C CNN
F 4 "04023J2R4BBS" H 5450 2900 50  0001 C CNN "Part Number"
	1    5450 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5A7D1DF5
P 6000 4900
F 0 "C5" H 6025 5000 50  0000 L CNN
F 1 "100pF" H 6025 4800 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad0.84x1.00mm_HandSolder" H 6038 4750 50  0001 C CNN
F 3 "" H 6000 4900 50  0001 C CNN
F 4 "CBR06C101J5GAC" H 0   0   50  0001 C CNN "Part Number"
	1    6000 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5A7D1DFC
P 6000 5050
F 0 "#PWR013" H 6000 4800 50  0001 C CNN
F 1 "GND" H 6000 4900 50  0000 C CNN
F 2 "" H 6000 5050 50  0001 C CNN
F 3 "" H 6000 5050 50  0001 C CNN
	1    6000 5050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5A7D1E02
P 7350 4350
F 0 "#PWR014" H 7350 4100 50  0001 C CNN
F 1 "GND" H 7350 4200 50  0000 C CNN
F 2 "" H 7350 4350 50  0001 C CNN
F 3 "" H 7350 4350 50  0001 C CNN
	1    7350 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR015
U 1 1 5A7D1E08
P 7800 4200
F 0 "#PWR015" H 7800 3950 50  0001 C CNN
F 1 "GND" H 7800 4050 50  0000 C CNN
F 2 "" H 7800 4200 50  0001 C CNN
F 3 "" H 7800 4200 50  0001 C CNN
	1    7800 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR016
U 1 1 5A7D1E0E
P 6450 3100
F 0 "#PWR016" H 6450 2850 50  0001 C CNN
F 1 "GND" H 6450 2950 50  0000 C CNN
F 2 "" H 6450 3100 50  0001 C CNN
F 3 "" H 6450 3100 50  0001 C CNN
	1    6450 3100
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR017
U 1 1 5A7D1E14
P 7500 3500
F 0 "#PWR017" H 7500 3250 50  0001 C CNN
F 1 "GND" H 7500 3350 50  0000 C CNN
F 2 "" H 7500 3500 50  0001 C CNN
F 3 "" H 7500 3500 50  0001 C CNN
	1    7500 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR018
U 1 1 5A7D1E1A
P 7900 3500
F 0 "#PWR018" H 7900 3250 50  0001 C CNN
F 1 "GND" H 7900 3350 50  0000 C CNN
F 2 "" H 7900 3500 50  0001 C CNN
F 3 "" H 7900 3500 50  0001 C CNN
	1    7900 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR019
U 1 1 5A7D1E20
P 5450 3100
F 0 "#PWR019" H 5450 2850 50  0001 C CNN
F 1 "GND" H 5450 2950 50  0000 C CNN
F 2 "" H 5450 3100 50  0001 C CNN
F 3 "" H 5450 3100 50  0001 C CNN
	1    5450 3100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR020
U 1 1 5A7D1E26
P 3500 3600
F 0 "#PWR020" H 3500 3350 50  0001 C CNN
F 1 "GND" H 3500 3450 50  0000 C CNN
F 2 "" H 3500 3600 50  0001 C CNN
F 3 "" H 3500 3600 50  0001 C CNN
	1    3500 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5A7D1E2C
P 7800 4050
F 0 "C4" H 7825 4150 50  0000 L CNN
F 1 "12pF" H 7825 3950 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad0.99x1.00mm_HandSolder" H 7838 3900 50  0001 C CNN
F 3 "" H 7800 4050 50  0001 C CNN
F 4 "06033J120GBTTR" H 7800 4050 50  0001 C CNN "Part Number"
	1    7800 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5A7D1E45
P 4200 3650
F 0 "R2" V 4280 3650 50  0000 C CNN
F 1 "1K" V 4200 3650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.84x1.00mm_HandSolder" V 4130 3650 50  0001 C CNN
F 3 "" H 4200 3650 50  0001 C CNN
F 4 "CRCW06031K00FKEA" H 0   0   50  0001 C CNN "Part Number"
	1    4200 3650
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5A7D1E4C
P 4200 3800
F 0 "R1" V 4280 3800 50  0000 C CNN
F 1 "510" V 4200 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.84x1.00mm_HandSolder" V 4130 3800 50  0001 C CNN
F 3 "" H 4200 3800 50  0001 C CNN
F 4 "CRT0603-FY-5100ELF" H 0   0   50  0001 C CNN "Part Number"
	1    4200 3800
	0    1    1    0   
$EndComp
$Comp
L Device:C C6
U 1 1 5A7D1E53
P 3950 4350
F 0 "C6" H 3975 4450 50  0000 L CNN
F 1 "100pF" H 3975 4250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad0.84x1.00mm_HandSolder" H 3988 4200 50  0001 C CNN
F 3 "" H 3950 4350 50  0001 C CNN
F 4 "CBR06C101J5GAC" H 0   0   50  0001 C CNN "Part Number"
	1    3950 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR024
U 1 1 5A7D1E5A
P 3950 4500
F 0 "#PWR024" H 3950 4250 50  0001 C CNN
F 1 "GND" H 3950 4350 50  0000 C CNN
F 2 "" H 3950 4500 50  0001 C CNN
F 3 "" H 3950 4500 50  0001 C CNN
	1    3950 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 4450 6000 4750
Wire Wire Line
	7350 3200 7500 3200
Wire Wire Line
	7350 3900 7200 3900
Wire Wire Line
	6900 3900 6450 3900
Wire Wire Line
	7350 4050 7350 3900
Wire Wire Line
	5750 2700 5750 3350
Wire Wire Line
	7050 3200 6750 3200
Wire Wire Line
	6750 3200 6750 3750
Wire Wire Line
	6750 3750 6450 3750
Wire Wire Line
	3950 3250 3950 3300
Wire Wire Line
	4050 3650 3950 3650
Connection ~ 3950 3650
Wire Wire Line
	4050 3800 3950 3800
Connection ~ 3950 3800
Wire Wire Line
	5250 3950 3950 3950
Connection ~ 3950 3950
Wire Wire Line
	4350 3650 5250 3650
Wire Wire Line
	5250 3800 4350 3800
Wire Wire Line
	3500 3300 3950 3300
Connection ~ 3950 3300
Text HLabel 4500 4100 0    60   Input ~ 0
PA_RF_IN
Text HLabel 8550 3900 2    60   Input ~ 0
PA_RF_OUT
Text HLabel 4850 1750 0    60   Input ~ 0
PowerDown
Text HLabel 6000 4600 0    60   Input ~ 0
PDET
$Comp
L Device:C C9
U 1 1 5A7D87E7
P 5000 2900
F 0 "C9" H 5025 3000 50  0000 L CNN
F 1 "1uF" H 5025 2800 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad0.84x1.00mm_HandSolder" H 5038 2750 50  0001 C CNN
F 3 "" H 5000 2900 50  0001 C CNN
F 4 "GRM188R61A105KA61ND" H 5000 2900 60  0001 C CNN "Part Number"
	1    5000 2900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR025
U 1 1 5A7D8882
P 5000 3100
F 0 "#PWR025" H 5000 2850 50  0001 C CNN
F 1 "GND" H 5000 2950 50  0000 C CNN
F 2 "" H 5000 3100 50  0001 C CNN
F 3 "" H 5000 3100 50  0001 C CNN
	1    5000 3100
	1    0    0    -1  
$EndComp
$Comp
L lsd-kicad:MMZ09332B U5
U 1 1 5A7DBC7A
P 5850 3900
F 0 "U5" H 6250 4400 60  0000 C CNN
F 1 "MMZ09332B" H 5500 3400 60  0000 C CNN
F 2 "lsf-kicad-lib:MMZ09332B" H 6150 4300 60  0001 C CNN
F 3 "https://www.nxp.com/docs/en/data-sheet/MMZ09332B.pdf" H 6250 4400 60  0001 C CNN
F 4 "MMZ09332BT1" H 0   0   50  0001 C CNN "Part Number"
	1    5850 3900
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR021
U 1 1 5A86744C
P 5500 1400
F 0 "#PWR021" H 5500 1250 50  0001 C CNN
F 1 "VCC" H 5517 1573 50  0000 C CNN
F 2 "" H 5500 1400 50  0001 C CNN
F 3 "" H 5500 1400 50  0001 C CNN
	1    5500 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 3650 3950 3800
Wire Wire Line
	3950 3800 3950 3950
Wire Wire Line
	3950 3950 3950 4200
Wire Wire Line
	3950 3300 3950 3650
Wire Wire Line
	4500 4100 4650 4100
Wire Wire Line
	4950 4100 5250 4100
Connection ~ 6450 3900
Wire Wire Line
	6450 3900 6450 4050
Text Label 4750 3800 0    50   ~ 0
VBA1
Text Label 4750 3650 0    50   ~ 0
VBA2
Text Label 5750 3150 0    50   ~ 0
PA_VCC1
Text Label 6750 3650 0    50   ~ 0
PA_VCC2
Text Label 6500 3900 0    50   ~ 0
PA_OUT
Connection ~ 7350 3900
Text Label 5000 4100 0    50   ~ 0
PA_In
Wire Wire Line
	6150 4450 6150 4500
Wire Wire Line
	6150 4500 6600 4500
Wire Wire Line
	6600 4500 6600 4700
$Comp
L power:GND #PWR058
U 1 1 5A969121
P 6600 4700
F 0 "#PWR058" H 6600 4450 50  0001 C CNN
F 1 "GND" H 6605 4527 50  0000 C CNN
F 2 "" H 6600 4700 50  0001 C CNN
F 3 "" H 6600 4700 50  0001 C CNN
	1    6600 4700
	1    0    0    -1  
$EndComp
Connection ~ 7500 3200
Wire Wire Line
	7500 3200 7900 3200
Connection ~ 7800 3900
Wire Wire Line
	7800 3900 8200 3900
Wire Wire Line
	7650 3900 7800 3900
$Comp
L power:GND #PWR0301
U 1 1 5AD6D78D
P 8200 4200
F 0 "#PWR0301" H 8200 3950 50  0001 C CNN
F 1 "GND" H 8200 4050 50  0000 C CNN
F 2 "" H 8200 4200 50  0001 C CNN
F 3 "" H 8200 4200 50  0001 C CNN
	1    8200 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C36
U 1 1 5AD6D794
P 8200 4050
F 0 "C36" H 8225 4150 50  0000 L CNN
F 1 "18pF" H 8225 3950 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad0.99x1.00mm_HandSolder" H 8238 3900 50  0001 C CNN
F 3 "" H 8200 4050 50  0001 C CNN
F 4 "06033J180GBT2A" H 8200 4050 50  0001 C CNN "Part Number"
	1    8200 4050
	1    0    0    -1  
$EndComp
Connection ~ 8200 3900
Wire Wire Line
	8200 3900 8550 3900
Wire Wire Line
	6450 3100 6450 3550
$Comp
L power:GND #PWR023
U 1 1 5AE4A7E4
P 6750 1950
F 0 "#PWR023" H 6750 1700 50  0001 C CNN
F 1 "GND" H 6750 1800 50  0000 C CNN
F 2 "" H 6750 1950 50  0001 C CNN
F 3 "" H 6750 1950 50  0001 C CNN
	1    6750 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 1750 5100 1750
Wire Wire Line
	6600 1850 6750 1850
Wire Wire Line
	6750 1850 6750 1950
Wire Wire Line
	5900 1550 5500 1550
Wire Wire Line
	5500 1550 5500 1400
Wire Wire Line
	6600 1550 6650 1550
Wire Wire Line
	7500 3200 7500 2900
Wire Wire Line
	7500 2900 7550 2900
$Comp
L Device:R R11
U 1 1 5AE4E00A
P 5700 2050
F 0 "R11" H 5770 2096 50  0000 L CNN
F 1 "124K" H 5770 2005 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.99x1.00mm_HandSolder" V 5630 2050 50  0001 C CNN
F 3 "~" H 5700 2050 50  0001 C CNN
F 4 "ERJ-3EKF1243V" H 0   0   50  0001 C CNN "Part Number"
	1    5700 2050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR022
U 1 1 5AE4E066
P 5700 2250
F 0 "#PWR022" H 5700 2000 50  0001 C CNN
F 1 "GND" H 5700 2100 50  0000 C CNN
F 2 "" H 5700 2250 50  0001 C CNN
F 3 "" H 5700 2250 50  0001 C CNN
	1    5700 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 2250 5700 2200
Wire Wire Line
	5700 1900 5700 1850
Wire Wire Line
	5700 1850 5900 1850
$Comp
L Device:R R4
U 1 1 5AE4F1FE
P 7300 1400
F 0 "R4" H 7370 1446 50  0000 L CNN
F 1 "100K" H 7370 1355 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.99x1.00mm_HandSolder" V 7230 1400 50  0001 C CNN
F 3 "~" H 7300 1400 50  0001 C CNN
F 4 "CRCW0603100KFKEA" H 0   0   50  0001 C CNN "Part Number"
	1    7300 1400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5AE4F266
P 7700 1400
F 0 "R5" H 7770 1446 50  0000 L CNN
F 1 "100K" H 7770 1355 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.99x1.00mm_HandSolder" V 7630 1400 50  0001 C CNN
F 3 "~" H 7700 1400 50  0001 C CNN
F 4 "CRCW0603100KFKEA" H 0   0   50  0001 C CNN "Part Number"
	1    7700 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 1750 7700 1750
Wire Wire Line
	7700 1750 7700 1550
Wire Wire Line
	7300 1550 7300 1650
Wire Wire Line
	7300 1650 6600 1650
$Comp
L power:VCC #PWR052
U 1 1 5AE50669
P 7300 1200
F 0 "#PWR052" H 7300 1050 50  0001 C CNN
F 1 "VCC" H 7317 1373 50  0000 C CNN
F 2 "" H 7300 1200 50  0001 C CNN
F 3 "" H 7300 1200 50  0001 C CNN
	1    7300 1200
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR053
U 1 1 5AE506A0
P 7700 1200
F 0 "#PWR053" H 7700 1050 50  0001 C CNN
F 1 "VCC" H 7717 1373 50  0000 C CNN
F 2 "" H 7700 1200 50  0001 C CNN
F 3 "" H 7700 1200 50  0001 C CNN
	1    7700 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 1200 7300 1250
Wire Wire Line
	7700 1200 7700 1250
Text HLabel 7700 1750 2    50   Input ~ 0
FLAGB
Text HLabel 7300 1650 2    50   Input ~ 0
PGOOD
Text Label 6650 1550 0    50   ~ 0
PA_VCC
Text Label 7550 2900 0    50   ~ 0
PA_VCC
Text Label 4900 2700 2    50   ~ 0
PA_VCC
Text Label 3950 3250 0    50   ~ 0
PA_VCC
$Comp
L lsd-kicad:FPF2700 U7
U 1 1 5AE4A6D8
P 6250 1700
F 0 "U7" H 6250 2115 50  0000 C CNN
F 1 "FPF2700" H 6250 2024 50  0000 C CNN
F 2 "lsf-kicad-lib:MLP-8" H 6250 1700 50  0001 C CNN
F 3 "https://www.fairchildsemi.com/datasheets/FP/FPF2701.pdf" H 6250 1700 50  0001 C CNN
F 4 "FPF2700MPX" H 0   0   50  0001 C CNN "Part Number"
	1    6250 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 2700 5450 2700
Wire Wire Line
	4900 2700 5000 2700
Wire Wire Line
	5000 2750 5000 2700
Connection ~ 5000 2700
Wire Wire Line
	5000 2700 5100 2700
Wire Wire Line
	5450 2700 5450 2750
Connection ~ 5450 2700
Wire Wire Line
	5450 2700 5750 2700
Wire Wire Line
	5450 3050 5450 3100
Wire Wire Line
	5000 3100 5000 3050
$Comp
L Device:R R12
U 1 1 5AE4FEF9
P 5100 1500
F 0 "R12" H 5170 1546 50  0000 L CNN
F 1 "100K" H 5170 1455 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.99x1.00mm_HandSolder" V 5030 1500 50  0001 C CNN
F 3 "~" H 5100 1500 50  0001 C CNN
F 4 "CRCW0603100KFKEA" H 0   0   50  0001 C CNN "Part Number"
	1    5100 1500
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR054
U 1 1 5AE4FF99
P 5100 1200
F 0 "#PWR054" H 5100 1050 50  0001 C CNN
F 1 "VCC" H 5117 1373 50  0000 C CNN
F 2 "" H 5100 1200 50  0001 C CNN
F 3 "" H 5100 1200 50  0001 C CNN
	1    5100 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 1200 5100 1350
Wire Wire Line
	5100 1650 5100 1750
Connection ~ 5100 1750
Wire Wire Line
	5100 1750 5900 1750
Text Label 5400 1750 0    50   ~ 0
FPF_ON
Text Label 5700 1850 0    50   ~ 0
ISET
Text Notes 4500 1950 0    50   ~ 0
ON: 0\nOFF: 1
$EndSCHEMATC
